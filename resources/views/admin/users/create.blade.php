@extends('admin.template.main') 
@section('title', 'Crear Usuario') 
@section('content') {!! Form::open(['route' => 'users.store',
'method' => 'POST']) !!}
<div class="form-group">
    {!! Form::label('name', 'Nombre') , Form::text('name', null, ['class' => 'form-control', 'required', 
    'placeholder' => 'Nombre completo']) !!}
</div>
<div class="form-group">
    {!! Form::label('email', 'Correo Electrónico') , Form::email('email', null, ['class' => 'form-control', 'required', 'placeholder' => 'asd@asd.asd']) !!}
</div>
<div class="form-group">
    {!! Form::label('password', 'Contraseña') , Form::password('password', ['class' => 'form-control', 'required', 'placeholder' => '*********']) !!}
</div>
<div class="form-group">
    {!! Form::label('type', 'Tipo Usuario') , Form::select('type',['member' => 'Miembro', 'admin' => 'Administrador'], null, ['placeholder' => 'Seleccione tipo', 'class' => 'form-control', 'required']) !!}
</div>
<div class="form-group mt-5">
    {!! Form::submit('Registrar Usuario', ['class' => 'btn btn-outline-success btn-block']) !!}
</div>
{!! Form::close() !!}
@endsection