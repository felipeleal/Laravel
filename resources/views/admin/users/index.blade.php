@extends('admin.template.main') 
@section('title', 'Lista de Usuarios') 
@section('content')
<div class="mb-3">
    <a role="button" class="btn btn-outline-success" href="{{ route('users.create') }}">Registrar usuario</a><br>
</div>
<div class="table-responsive">
    <table class="table table-hover">
        <thead class="thead-dark">
            <th scope="col">ID</th>
            <th scope="col">Nombre</th>
            <th scope="col">Email</th>
            <th scope="col">Tipo</th>
            <th scope="col">Fecha creación</th>
            <th scope="col">Fecha actualización</th>
            <th scope="col">Acciones</th>
        </thead>
        <tbody class="tbody-border">
            @foreach ($users as $user)
            <tr>
                <th scope="row">{{ $user->id }}</th>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>
                    @if ($user->type === "admin")
                    <button type="button" class="btn btn-primary btn-sm">{{ $user->type }}</button> @else
                    <button type="button" class="btn btn-secondary btn-sm">{{ $user->type }}</button> @endif
                </td>
                <td>{{ $user->created_at }}</td>
                <td>{{ $user->updated_at }}</td>
                <td>
                    <div class="btn-group" role="group" aria-label="acciones">
                        <a href="{{ route('users.get.destroy', $user->id) }}" role="button" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                        <a role="button" class="btn btn-warning"><i class="fas fa-edit"></i></a>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>


{{ $users->links() }}
@endsection