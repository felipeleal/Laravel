<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('assets/bower/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower/components-font-awesome/css/fontawesome-all.min.css') }}">
    <title>@yield('title', 'Default') | Panel de Administración</title>
</head>

<body>
    @include('admin.template.component.nav')
    <div class="container">
        <div class="row-fluid">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">@yield('title', 'Default')</h1>
            </div>
        </div>
        <div class="row-fluid">
    @include('flash::message') @yield('content')
        </div>
    </div>
    <script src="{{ asset('assets/bower/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/bower/bootstrap/dist/js/bootstrap.min.js') }}"></script>
</body>

</html>