<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="UTF-8">
	<title>{{ $article->title }}</title>
	<link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>

<body>
	<h1>Felipe Leal</h1>
	<hr> @if(true)
	<table>
		<tr>
			<td colspan="2">{{ $article->content }}</td>
		</tr>
		<tr>
			<td>{{ $article->user->name }}</td>
			<td>{{ $article->category->name }}</td>
		</tr>
		@foreach($article->tags as $tag) {{ $tag->name }} @endforeach
	</table>
	@endif
</body>

</html>