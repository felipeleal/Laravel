<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('articles/{nombre?}', function($nombre = 'defecto') {
//	return "Sección de articulos por return parametro recibido: {$nombre}";
//});

// Route::group(['prefix' => 'articles'], function(){
// 	Route::get('view/{id}',[
// 		'uses' => 'TestController@view',
// 		'as'   => 'articlesView'
// 	]);
// });

Route::group(['prefix' => 'admin'], function(){
    Route::resource('users', 'UsersController');
    Route::get('users/{id}/destroy', 'UsersController@destroy')->name('users.get.destroy')->where('id', '[0-9]+');
});
